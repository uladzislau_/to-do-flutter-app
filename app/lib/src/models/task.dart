import 'package:app/src/models/tag.dart';
import 'package:hive/hive.dart';

part 'task.g.dart';

@HiveType(typeId: 0)
class Task extends HiveObject {
  @HiveField(0)
  String title;

  @HiveField(1)
  String description;

  @HiveField(2)
  final HiveList<Tag> tags;

  @HiveField(3)
  int serialNumber;

  Task({
    required this.title,
    required this.description,
    required this.tags,
    required this.serialNumber,
  });
}
