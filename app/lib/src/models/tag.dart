import 'package:hive/hive.dart';

part 'tag.g.dart';

@HiveType(typeId: 1)
class Tag extends HiveObject {
  @HiveField(0)
  final String text;

  @HiveField(1)
  final int color;

  Tag({
    required this.text,
    required this.color,
  });
}
