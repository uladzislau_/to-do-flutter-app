import 'package:app/src/models/tag.dart';
import 'package:app/src/screens/tag_selector_dialog.dart';
import 'package:app/src/widgets/tag_list_widget.dart';
import 'package:app/src/widgets/tag_widget.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import '../models/task.dart';

class TaskScreen extends StatefulWidget {
  final Task task;

  TaskScreen(this.task);

  @override
  _TaskScreenState createState() => _TaskScreenState();
}

class _TaskScreenState extends State<TaskScreen> {
  late TextEditingController _titleController;
  late TextEditingController _descriptionController;
  late bool _isDragMode;

  @override
  void initState() {
    super.initState();
    _titleController = TextEditingController();
    _descriptionController = TextEditingController();
    _titleController.text = widget.task.title;
    _descriptionController.text = widget.task.description;
    _isDragMode = false;
  }

  @override
  void dispose() {
    _titleController.dispose();
    _descriptionController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        await _saveAndExit();
        return false;
      },
      child: GestureDetector(
        onTap: () {
          WidgetsBinding.instance?.focusManager.primaryFocus?.unfocus();
        },
        child: SafeArea(
          child: Scaffold(
            body: Padding(
              padding: EdgeInsets.all(16),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  // Task title (label)
                  Text('Title', style: const TextStyle(fontSize: 24, fontWeight: FontWeight.bold)),
                  // Separator
                  SizedBox(height: 8),
                  // Task title (edit text)
                  Container(
                    padding: EdgeInsets.all(16),
                    decoration: BoxDecoration(
                      color: Colors.cyan.shade50,
                      borderRadius: BorderRadius.circular(16),
                    ),
                    child: TextField(
                      controller: _titleController,
                      maxLines: 1,
                      textInputAction: TextInputAction.done,
                      decoration: InputDecoration.collapsed(hintText: 'Make a title...'),
                    ),
                  ),
                  // Separator
                  SizedBox(height: 16),
                  // Task description (label)
                  Text('Description', style: const TextStyle(fontSize: 24, fontWeight: FontWeight.bold)),
                  // Separator
                  SizedBox(height: 8),
                  // Task description (edit text)
                  Flexible(
                    child: Container(
                      padding: EdgeInsets.all(16),
                      decoration: BoxDecoration(
                        color: Colors.amber.shade50,
                        borderRadius: BorderRadius.circular(16),
                      ),
                      child: TextField(
                        controller: _descriptionController,
                        maxLines: 6,
                        textInputAction: TextInputAction.newline,
                        decoration: InputDecoration.collapsed(hintText: 'Descript...'),
                      ),
                    ),
                    flex: 2,
                  ),
                  // Separator
                  SizedBox(height: 16),
                  // Task's tags (label & button)
                  Row(
                    children: [
                      // Label
                      Expanded(child: Text('Tags', style: const TextStyle(fontSize: 24, fontWeight: FontWeight.bold))),
                      // Separator
                      SizedBox(width: 8),
                      // `Add tag` button
                      GestureDetector(
                        child: Icon(Icons.add_circle, size: 40, color: Colors.red),
                        onTap: () => _addTag(
                          context,
                          onTagSelected: (tag) async {
                            var tags = widget.task.tags;
                            if (!tags.contains(tag)) tags.add(tag);
                            await widget.task.save();
                            Navigator.of(context).pop();
                          },
                        ),
                      ),
                    ],
                  ),
                  Divider(),
                  // Separator
                  SizedBox(height: 8),
                  // Task's tags (refreshable)
                  Expanded(
                    child: ValueListenableBuilder(
                      valueListenable: Hive.box<Task>('tasks').listenable(),
                      builder: (context, Box<Task> box, _) {
                        var task = box.values.elementAt(widget.task.serialNumber);
                        if (task.tags.isEmpty) return Container();
                        return SingleChildScrollView(
                          child: TagListWidget(
                            task.tags,
                            create: (tag, _) => LongPressDraggable<Tag>(
                              data: tag,
                              onDragStarted: () => setState(() {
                                _isDragMode = true;
                              }),
                              onDragEnd: (_) => setState(() {
                                _isDragMode = false;
                              }),
                              feedback: Material(
                                type: MaterialType.transparency,
                                child: TagWidget(text: tag.text, color: Color(tag.color).withOpacity(0.5)),
                              ),
                              child: TagWidget(text: tag.text, color: Color(tag.color)),
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                ],
              ),
            ),
            floatingActionButton: _isDragMode
                ? DragTarget<Tag>(
                    builder: (context, candidateItems, __) {
                      double size = candidateItems.isEmpty ? 56 : 72;
                      return Container(
                        width: size,
                        height: size,
                        child: FloatingActionButton(
                          backgroundColor: Colors.red,
                          child: (candidateItems.isEmpty)
                              ? Icon(Icons.delete, size: 32, color: Colors.white)
                              : Icon(Icons.delete_forever, size: 48, color: Colors.white),
                          onPressed: null,
                        ),
                      );
                    },
                    onAccept: (tag) {
                      widget.task.tags.remove(tag);
                      widget.task.save();
                    },
                  )
                : FloatingActionButton(
                    backgroundColor: Colors.green,
                    child: Icon(Icons.check, size: 32, color: Colors.white),
                    onPressed: () async {
                      await _saveAndExit();
                    },
                  ),
          ),
        ),
      ),
    );
  }

  Future<void> _saveAndExit() async {
    widget.task.title = _titleController.text;
    widget.task.description = _descriptionController.text;
    await widget.task.save();
    Navigator.of(context).pop();
  }
}

void _addTag(BuildContext context, {void Function(Tag)? onTagSelected}) {
  showDialog(
    context: context,
    builder: (context) => Center(
      child: TagSelectorDialog(
        onTagSelected: onTagSelected,
      ),
    ),
  );
}
