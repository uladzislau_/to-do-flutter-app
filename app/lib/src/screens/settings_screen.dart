import 'package:app/src/models/tag.dart';
import 'package:app/src/screens/tag_selector_dialog.dart';
import 'package:app/src/widgets/tag_list_widget.dart';
import 'package:app/src/widgets/tag_widget.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';

class SettingsScreen extends StatefulWidget {
  final void Function(List<Tag>)? onRemoveTags;

  SettingsScreen({this.onRemoveTags});

  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  late bool _isDragMode;
  late List<Tag> removedTags;

  @override
  void initState() {
    super.initState();
    _isDragMode = false;
    removedTags = [];
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: SafeArea(
        child: Scaffold(
          body: Padding(
            padding: EdgeInsets.all(16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                // All tags (label & button)
                Row(
                  children: [
                    // Label
                    Expanded(child: Text('All tags', style: const TextStyle(fontSize: 24, fontWeight: FontWeight.bold))),
                    // Separator
                    SizedBox(width: 8),
                    // `Add tag` button
                    GestureDetector(
                      child: Icon(Icons.add_circle, size: 40, color: Colors.red),
                      onTap: () => _createTag(context),
                    ),
                  ],
                ),
                Divider(),
                // Separator
                SizedBox(height: 8),
                // Task's tags (refreshable)
                Expanded(
                  child: ValueListenableBuilder(
                    valueListenable: Hive.box<Tag>('tags').listenable(),
                    builder: (context, Box<Tag> box, _) {
                      if (box.values.isEmpty) return Container();
                      return SingleChildScrollView(
                        child: TagListWidget(
                          box.values.toList(),
                          create: (tag, _) => LongPressDraggable<Tag>(
                            data: tag,
                            onDragStarted: () => setState(() {
                              _isDragMode = true;
                            }),
                            onDragEnd: (_) => setState(() {
                              _isDragMode = false;
                            }),
                            feedback: Material(
                              type: MaterialType.transparency,
                              child: TagWidget(text: tag.text, color: Color(tag.color).withOpacity(0.5)),
                            ),
                            child: TagWidget(text: tag.text, color: Color(tag.color)),
                          ),
                        ),
                      );
                    },
                  ),
                ),
              ],
            ),
          ),
          floatingActionButton: _isDragMode
              ? DragTarget<Tag>(
                  builder: (context, candidateItems, __) {
                    double size = candidateItems.isEmpty ? 56 : 72;
                    return Container(
                      width: size,
                      height: size,
                      child: FloatingActionButton(
                        backgroundColor: Colors.red,
                        child: (candidateItems.isEmpty)
                            ? Icon(Icons.delete, size: 32, color: Colors.white)
                            : Icon(Icons.delete_forever, size: 48, color: Colors.white),
                        onPressed: null,
                      ),
                    );
                  },
                  onAccept: (tag) {
                    removedTags.add(tag);
                    tag.delete();
                  },
                )
              : FloatingActionButton(
                  backgroundColor: Colors.green,
                  child: Icon(Icons.check, size: 32, color: Colors.white),
                  onPressed: () {
                    widget.onRemoveTags?.call(removedTags);
                    Navigator.of(context).pop();
                  },
                ),
        ),
      ),
      onWillPop: () async {
        widget.onRemoveTags?.call(removedTags);
        return true;
      },
    );
  }
}

void _createTag(BuildContext context) {
  showDialog(
    context: context,
    builder: (context) => Center(
      child: TagSelectorDialog(showCreatorOnly: true),
    ),
  );
}
