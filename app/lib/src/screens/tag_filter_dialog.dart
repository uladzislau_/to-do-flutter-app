import 'package:app/src/models/tag.dart';
import 'package:app/src/widgets/tag_list_widget.dart';
import 'package:app/src/widgets/tag_widget.dart';
import 'package:flutter/material.dart';

class TagFilterDialog extends StatefulWidget {
  final List<Tag> tags;
  final List<Tag> initialFilteredTags;
  final void Function(List<Tag>)? onFilterApplied;

  TagFilterDialog({
    required this.tags,
    required this.initialFilteredTags,
    this.onFilterApplied,
  });

  @override
  _TagFilterDialogState createState() => _TagFilterDialogState();
}

class _TagFilterDialogState extends State<TagFilterDialog> {
  late List<bool> selectedTags;

  @override
  void initState() {
    super.initState();
    selectedTags = List.generate(widget.tags.length, (i) => widget.initialFilteredTags.contains(widget.tags[i]));
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 32, vertical: 64),
      padding: EdgeInsets.all(16),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(16),
      ),
      child: Material(
        color: Colors.white,
        child: _build(context),
      ),
    );
  }

  Widget _build(BuildContext context) {
    // Existing tags
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            // `Close` button
            GestureDetector(
              child: Icon(Icons.close_rounded, size: 32, color: Colors.red),
              onTap: () => Navigator.of(context).pop(),
            ),
            // Separator
            SizedBox(width: 8),
            // Label
            Expanded(child: Text('Filter tags', style: const TextStyle(fontSize: 24, fontWeight: FontWeight.bold))),
            // Separator
            SizedBox(width: 8),
            // `Apply filters` button
            GestureDetector(
              child: Icon(Icons.done, size: 32, color: Colors.blue),
              onTap: () {
                List<Tag> filteredTags = [];
                widget.tags.asMap().forEach((i, tag) {
                  if (selectedTags[i]) filteredTags.add(tag);
                });
                widget.onFilterApplied?.call(filteredTags);
                Navigator.of(context).pop();
              },
            ),
          ],
        ),
        Divider(),
        // Separator
        SizedBox(height: 8),
        // Tags
        Expanded(
          child: SingleChildScrollView(
            child: (widget.tags.isEmpty)
                ? Container()
                : TagListWidget(
                    widget.tags,
                    create: (tag, i) => GestureDetector(
                      child: TagWidget(text: tag.text, color: Color(tag.color), isSelected: selectedTags[i]),
                      onTap: () {
                        setState(() {
                          selectedTags[i] = !selectedTags[i];
                        });
                      },
                    ),
                  ),
          ),
        ),
      ],
    );
  }
}
