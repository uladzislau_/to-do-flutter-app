import 'package:app/src/models/tag.dart';
import 'package:app/src/widgets/color_palette_widget.dart';
import 'package:app/src/widgets/tag_list_widget.dart';
import 'package:app/src/widgets/tag_widget.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';

class TagSelectorDialog extends StatefulWidget {
  final void Function(Tag)? onTagSelected;
  final bool showCreatorOnly;

  TagSelectorDialog({
    this.onTagSelected,
    this.showCreatorOnly = false,
  });

  @override
  _TagSelectorDialogState createState() => _TagSelectorDialogState();
}

class _TagSelectorDialogState extends State<TagSelectorDialog> {
  bool _isTagCreationMode = false;
  late Color _selectedColor;
  late TextEditingController _controller;

  @override
  void initState() {
    super.initState();
    _controller = TextEditingController();
    if (widget.showCreatorOnly) _isTagCreationMode = true;
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 32, vertical: 64),
      padding: EdgeInsets.all(16),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(16),
      ),
      child: Material(
        color: Colors.white,
        child: _isTagCreationMode ? _buildTagCreationPage(context) : _buildTagSelectionPage(context),
      ),
    );
  }

  Widget _buildTagSelectionPage(BuildContext context) {
    // Existing tags
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            // `Close` button
            GestureDetector(
              child: Icon(Icons.close_rounded, size: 32, color: Colors.red),
              onTap: () => Navigator.of(context).pop(),
            ),
            // Separator
            SizedBox(width: 8),
            // Label
            Expanded(child: Text('Add tag', style: const TextStyle(fontSize: 24, fontWeight: FontWeight.bold))),
            // Separator
            SizedBox(width: 8),
            // `Create tag` button
            GestureDetector(
              child: Icon(Icons.construction, size: 32, color: Colors.blue),
              onTap: () => setState(() {
                _isTagCreationMode = true;
                _controller.text = '';
              }),
            ),
          ],
        ),
        Divider(),
        // Separator
        SizedBox(height: 8),
        // Tags
        Expanded(
          child: ValueListenableBuilder(
            valueListenable: Hive.box<Tag>('tags').listenable(),
            builder: (context, Box<Tag> box, _) {
              return SingleChildScrollView(
                child: (box.values.isEmpty)
                    ? Container()
                    : TagListWidget(
                        box.values.toList(),
                        create: (tag, _) => GestureDetector(
                          child: TagWidget(text: tag.text, color: Color(tag.color)),
                          onTap: () => widget.onTagSelected?.call(tag),
                        ),
                      ),
              );
            },
          ),
        ),
      ],
    );
  }

  Widget _buildTagCreationPage(BuildContext context) {
    // Existing tags
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            // `Close` button
            GestureDetector(
              child: Icon(widget.showCreatorOnly ? Icons.close : Icons.arrow_back, size: 32, color: Colors.red),
              onTap: () => setState(() {
                if (widget.showCreatorOnly)
                  Navigator.of(context).pop();
                else
                  _isTagCreationMode = false;
              }),
            ),
            // Separator
            SizedBox(width: 8),
            // Label
            Expanded(child: Text('Create tag', style: const TextStyle(fontSize: 24, fontWeight: FontWeight.bold))),
            // Separator
            SizedBox(width: 8),
            // `Create tag` button
            GestureDetector(
              child: Icon(Icons.done, size: 32, color: Colors.blue),
              onTap: () async {
                var text = _controller.text;
                if (text.isEmpty) text = 'Tag';
                var tags = Hive.box<Tag>('tags');
                await tags.add(Tag(text: text, color: _selectedColor.value));
                setState(() {
                  if (widget.showCreatorOnly)
                    Navigator.of(context).pop();
                  else
                    _isTagCreationMode = false;
                });
              },
            ),
          ],
        ),
        Divider(),
        // Separator
        SizedBox(height: 8),
        // Tag name input field
        Container(
          padding: EdgeInsets.all(16),
          decoration: BoxDecoration(
            color: Colors.amber.shade50,
            borderRadius: BorderRadius.circular(16),
          ),
          child: TextField(
            maxLines: 1,
            controller: _controller,
            textInputAction: TextInputAction.done,
            autofocus: false,
            decoration: InputDecoration.collapsed(hintText: 'Tag name'),
          ),
        ),
        // Separator
        SizedBox(height: 16),
        // Color palette
        Expanded(
          child: ColorPaletteWidget(
            initialSelectedIndex: 0,
            onColorSelected: (color) => _selectedColor = color,
          ),
        ),
      ],
    );
  }
}
