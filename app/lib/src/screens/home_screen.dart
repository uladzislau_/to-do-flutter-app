import 'package:app/src/models/tag.dart';
import 'package:app/src/models/task.dart';
import 'package:app/src/screens/tag_filter_dialog.dart';
import 'package:app/src/screens/task_screen.dart';
import 'package:app/src/widgets/task_list_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'settings_screen.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  late List<Tag> filteredTags;

  @override
  void initState() {
    super.initState();
    filteredTags = [];
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Column(
          children: [
            // Top panel
            Padding(
              padding: EdgeInsets.only(left: 16, right: 16, top: 16),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  GestureDetector(
                    child: Icon(Icons.filter_list, size: 32, color: Colors.indigo),
                    onTap: () => _openFilterDialog(context, Hive.box<Tag>('tags').values.toList()),
                  ),
                  SizedBox(width: 12),
                  GestureDetector(
                    child: Icon(Icons.settings, size: 32, color: Colors.indigo),
                    onTap: () => _openSettings(context),
                  ),
                ],
              ),
            ),
            // Separator
            SizedBox(height: 8),
            // Task list
            Expanded(
              child: ValueListenableBuilder(
                valueListenable: Hive.box<Task>('tasks').listenable(),
                builder: (context, Box<Task> box, _) {
                  // Filter tasks
                  var filteredTasks = filteredTags.isNotEmpty
                      ? box.values
                          .where(
                            (task) => task.tags.any(
                              (tag) => filteredTags.contains(tag),
                            ),
                          )
                          .toList()
                      : box.values.toList();
                  if (filteredTasks.isEmpty) {
                    return Center(
                      child: Text('No tasks', style: const TextStyle(fontSize: 24, color: Colors.grey)),
                    );
                  }
                  return TaskListWidget(
                    filteredTasks..sort((t1, t2) => t1.serialNumber.compareTo(t2.serialNumber)),
                    onTaskPressed: (task) => _openTaskScreen(context, task),
                  );
                },
              ),
            ),
          ],
        ),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add, size: 32, color: Colors.white),
          backgroundColor: Colors.indigo,
          onPressed: () => _openTaskScreen(context, null),
        ),
      ),
    );
  }

  void _openFilterDialog(BuildContext context, List<Tag> tags) {
    showDialog(
      context: context,
      builder: (context) => TagFilterDialog(
        tags: tags,
        initialFilteredTags: filteredTags,
        onFilterApplied: (filtered) => setState(() {
          filteredTags = filtered;
        }),
      ),
    );
  }

  void _openSettings(BuildContext context) {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => SettingsScreen(
          onRemoveTags: (removed) => setState(() {
            filteredTags.removeWhere((tag) => removed.contains(tag));
          }),
        ),
      ),
    );
  }
}

void _openTaskScreen(BuildContext context, Task? task) async {
  // Creates new task and save it
  if (task == null) {
    var tags = Hive.box<Tag>('tags');
    var tasks = Hive.box<Task>('tasks');
    task = Task(title: '', description: '', tags: HiveList(tags), serialNumber: tasks.values.length);
    await tasks.add(task);
  }
  // Shows tasks details
  Navigator.of(context).push(
    MaterialPageRoute(
      builder: (context) => TaskScreen(task!),
    ),
  );
}
