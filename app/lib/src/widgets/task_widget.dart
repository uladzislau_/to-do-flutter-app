import 'package:app/src/widgets/tag_widget.dart';
import 'package:flutter/material.dart';
import 'tag_list_widget.dart';
import '../models/task.dart';

class TaskWidget extends StatelessWidget {
  final Task task;

  TaskWidget(this.task);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(16),
      decoration: BoxDecoration(
        color: Colors.indigo.shade50,
        borderRadius: BorderRadius.circular(8),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          // Tags (if exists)
          if (task.tags.isNotEmpty) ...[
            // Tags
            TagListWidget(
              task.tags,
              create: (tag, _) => TagWidget(text: tag.text, color: Color(tag.color)),
            ),
            // Separator
            SizedBox(height: 16),
          ],
          // Task title
          Text(
            task.title,
            style: const TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            maxLines: 2,
            softWrap: false,
            overflow: TextOverflow.fade,
          ),
        ],
      ),
    );
  }
}
