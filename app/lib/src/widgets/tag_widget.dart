import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class TagWidget extends StatelessWidget {
  final String text;
  final Color color;
  final bool isSelected;

  TagWidget({
    required this.text,
    required this.color,
    this.isSelected = false,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(4),
      decoration: BoxDecoration(
        color: color,
        borderRadius: BorderRadius.circular(8),
        border: isSelected ? Border.all(color: Colors.black, width: 2) : null,
      ),
      child: Text(
        text,
        softWrap: true,
        overflow: TextOverflow.fade,
        style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500, color: Colors.black),
      ),
    );
  }
}
