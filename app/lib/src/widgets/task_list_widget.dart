import 'dart:math';
import 'package:app/src/widgets/task_widget.dart';
import 'package:flutter/material.dart';
import '../models/task.dart';

class TaskListWidget extends StatelessWidget {
  final List<Task> tasks;
  final void Function(Task)? onTaskPressed;

  TaskListWidget(
    this.tasks, {
    this.onTaskPressed,
  });

  Widget build(BuildContext context) {
    return ReorderableListView.builder(
      itemCount: tasks.length,
      itemBuilder: (context, i) => Dismissible(
        key: ValueKey(tasks[i].key),
        child: GestureDetector(
          child: Container(
            width: double.infinity,
            margin: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
            child: TaskWidget(tasks[i]),
          ),
          behavior: HitTestBehavior.translucent,
          onTap: () => onTaskPressed?.call(tasks[i]),
        ),
        background: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(colors: [Colors.white, Colors.red]),
          ),
        ),
        secondaryBackground: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(colors: [Colors.red, Colors.white]),
          ),
        ),
        onDismissed: (direction) async {
          for (var k = i + 1; k < tasks.length; k++) tasks[k].serialNumber--;
          for (var k = i + 1; k < tasks.length; k++) await tasks[k].save();
          await tasks[i].delete();
        },
      ),
      proxyDecorator: (_, i, __) => TaskWidget(tasks[i]),
      onReorder: (oldIndex, newIndex) async {
        if (newIndex > oldIndex) {
          newIndex--;
          for (var i = oldIndex + 1; i <= newIndex; i++) tasks[i].serialNumber--;
          tasks[oldIndex].serialNumber = tasks[newIndex].serialNumber + 1;
        } else {
          for (var i = newIndex; i <= oldIndex - 1; i++) tasks[i].serialNumber++;
          tasks[oldIndex].serialNumber = tasks[newIndex].serialNumber - 1;
        }
        for (var i = min(oldIndex, newIndex); i <= max(oldIndex, newIndex); i++) await tasks[i].save();
      },
    );
  }
}
