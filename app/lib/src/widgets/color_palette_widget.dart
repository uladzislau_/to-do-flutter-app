import 'package:flutter/material.dart';

var _colors = [
  Colors.white,
  Colors.yellow,
  Colors.lime,
  Colors.amber,
  Colors.red,
  Colors.pink,
  Colors.green,
  Colors.teal,
  Colors.cyan,
  Colors.blue,
  Colors.indigo,
  Colors.purple,
  Colors.brown,
];

class ColorPaletteWidget extends StatefulWidget {
  final int initialSelectedIndex;
  final void Function(Color)? onColorSelected;

  ColorPaletteWidget({
    required this.initialSelectedIndex,
    this.onColorSelected,
  });

  @override
  _ColorPaletteWidgetState createState() => _ColorPaletteWidgetState();
}

class _ColorPaletteWidgetState extends State<ColorPaletteWidget> {
  late int _selectedIndex;

  @override
  void initState() {
    super.initState();
    _selectedIndex = widget.initialSelectedIndex;
    widget.onColorSelected?.call(_colors[_selectedIndex]);
  }

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
        maxCrossAxisExtent: 48,
        crossAxisSpacing: 8,
        mainAxisSpacing: 8,
      ),
      itemCount: _colors.length,
      itemBuilder: (context, i) => GestureDetector(
        child: ColorWidget(_colors[i], _selectedIndex == i),
        onTap: () {
          if (i != _selectedIndex) {
            widget.onColorSelected?.call(_colors[i]);
            setState(() {
              _selectedIndex = i;
            });
          }
        },
      ),
    );
  }
}

class ColorWidget extends StatelessWidget {
  final Color color;
  final bool isSelected;

  ColorWidget(this.color, this.isSelected);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 40,
      decoration: BoxDecoration(
        color: color,
        shape: BoxShape.circle,
      ),
      child: isSelected ? Icon(Icons.check) : null,
    );
  }
}
