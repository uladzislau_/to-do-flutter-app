import 'package:flutter/material.dart';
import '../models/tag.dart';

class TagListWidget extends StatelessWidget {
  final List<Tag> tags;
  final Widget Function(Tag, int) create;

  TagListWidget(
    this.tags, {
    required this.create,
  });

  @override
  Widget build(BuildContext context) {
    return Wrap(
      spacing: 8,
      runSpacing: 8,
      children: [for (var i = 0; i < tags.length; i++) create(tags[i], i)],
    );
  }
}
