import 'package:app/src/models/tag.dart';
import 'package:app/src/models/task.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'src/app.dart';

void _addInitialData() {
  var tasks = Hive.box<Task>('tasks');
  var tags = Hive.box<Tag>('tags');

  var coding = Tag(text: 'Coding', color: Colors.blue.shade200.value);
  var eating = Tag(text: 'Eating', color: Colors.red.shade200.value);
  var walking = Tag(text: 'Walking', color: Colors.green.shade200.value);

  tags.put(0, coding);
  tags.put(1, eating);
  tags.put(2, walking);

  for (var i = 0; i < 1000; i++) {
    var task = Task(title: 'Task number ${i + 1}', description: '', tags: HiveList(tags, objects: [coding, eating, walking]), serialNumber: i);
    tasks.put(i, task);
  }
}

void main() async {
  await Hive.initFlutter();
  Hive.registerAdapter(TaskAdapter());
  Hive.registerAdapter(TagAdapter());
  await Hive.openBox<Tag>('tags');
  await Hive.openBox<Task>('tasks');
  // _addInitialData();
  runApp(App());
}
